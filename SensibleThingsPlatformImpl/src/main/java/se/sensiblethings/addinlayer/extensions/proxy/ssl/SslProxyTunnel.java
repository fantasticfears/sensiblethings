/*
 * Copyright 2013 The SensibleThings Consortium
 * This file is part of The SensibleThings Platform.
 *
 * The SensibleThings Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The SensibleThings Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The SensibleThings Platform.  If not, see <http://www.gnu.org/licenses/>.
 */

package se.sensiblethings.addinlayer.extensions.proxy.ssl;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.logging.Level;

import se.sensiblethings.addinlayer.extensions.proxy.ProxyExtension;
import se.sensiblethings.addinlayer.extensions.proxy.ProxyTunnel;
import se.sensiblethings.disseminationlayer.communication.Communication;
import se.sensiblethings.disseminationlayer.communication.DestinationNotReachableException;
import se.sensiblethings.disseminationlayer.communication.Message;
import se.sensiblethings.disseminationlayer.communication.proxy.ProxyPayload;
import se.sensiblethings.disseminationlayer.communication.ssl.SslCommunication;

public class SslProxyTunnel extends ProxyTunnel {

	public SslProxyTunnel(ProxyExtension parent, long sessionID, Socket socket,
			ObjectInputStream in, ObjectOutputStream out) {
		super(parent, sessionID, socket, in, out);
	}
	
	@Override
	protected Communication createCommunicationLayer() {
		return new SslGatewayCommunication();
	}

	private class SslGatewayCommunication extends SslCommunication {
		// This always uses the SSL communication

		public SslGatewayCommunication() {
			super();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * se.mediasense.disseminationlayer.communication.rudp.RUDPCommunication
		 * #sendMessage(se.mediasense.disseminationlayer.communication.Message)
		 */
		@Override
		public void sendMessage(Message message) throws DestinationNotReachableException {
			try {
				super.sendMessage(message);
			}  catch (RuntimeException e) {
				 log(Level.INFO, "Irrecoverable runtime error in one proxyTunnel communication-layer, printing stacktrace and closing tunnel");
				log(Level.INFO, e);
				closeTunnel();
			}
		}
                
                
		@Override
		protected synchronized void handleConnection(Socket s) {
			try {
                                
				byte[] buffer = new byte[2048];
				int numberOfReadBytes = 0;
				int position = 0;			
				InputStream is = s.getInputStream();			
				do {
					numberOfReadBytes = is.read(buffer, position, buffer.length-position);				
					position = position + numberOfReadBytes;
					if(buffer.length-position <=0){
						buffer = Arrays.copyOf(buffer, buffer.length*2);
						
					}
				} while (numberOfReadBytes != -1);
				is.close();
				s.close();

				//String stringRepresentation = new String(buffer);

				Message message = messageSerializer.deserializeMessage(buffer);
				
				// Instead of sending the message to the PostOffice we send it over
				// the tunnel
				byte[] data = messageSerializer.serializeMessage(message);
				if (data == null) {
					System.out.println("OUT:    serializeMessage returned null!!!");
				}
				ProxyPayload p = new ProxyPayload(getSessionID(), data, message.getFromNode(),
						message.getToNode());
				writePayloadToTunnel(p);
                                p=null;
				is.close();
				s.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}




}
