/*
 * Copyright 2013 The SensibleThings Consortium
 * This file is part of The SensibleThings Platform.
 *
 * The SensibleThings Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The SensibleThings Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The SensibleThings Platform.  If not, see <http://www.gnu.org/licenses/>.
 */

package se.sensiblethings.addinlayer.extensions.proxy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.net.Socket;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Random;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import se.sensiblethings.disseminationlayer.communication.Communication;
import se.sensiblethings.disseminationlayer.communication.CommunicationStateListener;
import se.sensiblethings.disseminationlayer.communication.DestinationNotReachableException;
import se.sensiblethings.disseminationlayer.communication.Message;
import se.sensiblethings.disseminationlayer.communication.MessageSerializer;
import se.sensiblethings.disseminationlayer.communication.proxy.ProxyPayload;
import se.sensiblethings.disseminationlayer.communication.serializer.ObjectSerializer;
import se.sensiblethings.interfacelayer.SensibleThingsNode;

public abstract class ProxyTunnel implements Runnable {

	protected static final long RECONNECTION_TIMEOUT = 1000 * 15;
	private static final long KEEPALIVE_TIME = 30*1000;
	TimeOutConnectionOnExceptionHandler timeOutConnectionOnExceptionHandler = new TimeOutConnectionOnExceptionHandler();
	private Socket socket;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	private Communication gatewayCommunication;
	protected MessageSerializer messageSerializer = new ObjectSerializer();
	private boolean running = true;
	private long sessionID = 0;
	private Thread mainThread;
	private Thread keepAliveThread;
	private Thread timeoutThread;
	private ProxyExtension parent;
	private Deque<ProxyPayload> backlog;
	private boolean inRecovery;
	private long lastRecievedMessage = System.currentTimeMillis();
	private static Random random = new Random();
	private static Logger logger = Logger.getLogger(ProxyTunnel.class.getName());

	public static void SetFileLogger() {
	    //Initial Setup
	    //Filelogger, for debugging purposes
	    //logger = Logger.getLogger(ProxyTunnel.class.getName());
	    //Disables Console Output for Errors
	    logger.setUseParentHandlers(false);
	    FileHandler fh;
	    try {
	        fh = new FileHandler("./log/proxyTunnel%g.log", 1024*1024*3,5);
	        
	        fh.setLevel(Level.ALL);
	        logger.addHandler(fh);
	        logger.setLevel(Level.ALL);
	        SimpleFormatter formatter = new SimpleFormatter();
	        fh.setFormatter(formatter);
	        logger.log(Level.INFO, "Logging Started.");
	    } catch (IOException ex) {
	        log(Level.SEVERE, null, ex);
	    } catch (SecurityException ex) {
	        log(Level.SEVERE, null, ex);
	    }
	}

	protected static void log(Level logtype, String logtext, Exception logex) {
		logger.log(logtype, logtext, logex);
	}

	protected static void log(Level logtype, String logtext) {
		logger.log(logtype, logtext);
	}

	protected static void log(Level logtype, Exception e) {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		e.printStackTrace(new PrintStream(output, true));
		log(logtype, output.toString());
	}

	
	final class TimeOutConnectionOnExceptionHandler implements
			UncaughtExceptionHandler {
		public void uncaughtException(Thread thread, Throwable exception) {
			if (thread == ProxyTunnel.this.mainThread) {
				if (running) {
					log(Level.WARNING,
							"ProxyTunnel caught an unhandled exception while running, stopping tunnel");
					closeTunnel();
				}
			}
		}
	}
	
	
	public ProxyTunnel(ProxyExtension parent, long sessionID, Socket socket,
			ObjectInputStream in, ObjectOutputStream out) {
		this.parent = parent;
		this.sessionID = sessionID;
		if(socket == null){
			throw new RuntimeException("socket is null!!");
		}
		this.socket = socket;
		this.inRecovery = false;
		// Open up the tunnel
		this.in = in;
		this.out = out;

		// The communication interface to the Internet
		gatewayCommunication = createCommunicationLayer();
		gatewayCommunication.addStateListener(new CommunicationStateListener() {
			
			public void onNewCommunicationState(Communication communication,
					String state) {
				//Debugging nullpointer
				if(gatewayCommunication == null){
					gatewayCommunication = communication;
				}
				if(state.equals(Communication.COMMUNICATION_STATE_CONNECTED)){
					createAndStartProxyTunnelThread();
					createAndStartKeepAliveThread();
				}
				
			}
		});

	}
	
	protected abstract Communication createCommunicationLayer();

	
	private void createAndStartProxyTunnelThread() {
		mainThread = new Thread(this);
		mainThread.setName("Tunnel:" + getLocalAddress());
		mainThread.setUncaughtExceptionHandler(timeOutConnectionOnExceptionHandler);
		log(Level.INFO, "Tunnel opened for " + socket.getRemoteSocketAddress().toString() + " to " + getLocalAddress());		
		mainThread.start();
		
	}

	private void createAndStartKeepAliveThread() {
		log(Level.FINEST, "Starting Keepalive thread for: " + getLocalAddress());
		if(keepAliveThread != null){
			log(Level.FINEST, "Old keepalive thread found, replacing");
			Thread oldThread = keepAliveThread;
			keepAliveThread = null;
			oldThread.interrupt();
			
		}
		keepAliveThread = new Thread(new KeepAliveThread(), "keepAliveThread for Tunnel:" + getLocalAddress());
		keepAliveThread.start();
	}

	
	public void run() {
		ProxyPayload payload, incommingPayload;
		
		
		// Send the tunnel address to remote endpoint
		payload = new ProxyPayload(sessionID, null,
				gatewayCommunication.getLocalSensibleThingsNode(),
				gatewayCommunication.getLocalSensibleThingsNode());
		writePayloadToTunnel(payload);
		payload = null;
		// The Tunnel is now operational
		
		
		// Resume transmission of lost payloads
		handleBacklog();
		
		//Main loop
		Message message = null;
		while (running) {
			
			try {
				// Read a message from the other side of the tunnel
				incommingPayload = (ProxyPayload) in.readUnshared();
				// System.out.println("inc: " + str);
			}
			catch(Exception e){
				if (Thread.currentThread() == mainThread){
				log(Level.FINER, "Tunnel exception for " + socket.getRemoteSocketAddress().toString() + " to " + getLocalAddress(), e);
				closeTunnel();
				}
				return;
			}
			try{
				
				if(incommingPayload.getSessionID() != sessionID){
					log(Level.INFO, "Remote shutdown detected, closing tunnel:" + getLocalAddress() + " from " + getRemoteAddress());
					this.shutdown();
					return;
				}
				lastRecievedMessage = System.currentTimeMillis();
				if(incommingPayload.getMessage() == null){
					continue;
				}
				
				// And send it to the Internet
				message = messageSerializer
						.deserializeMessage(incommingPayload.getMessage());
				gatewayCommunication.sendMessage(message);
				
			} catch (DestinationNotReachableException e) {
				//In case the message cannot be sent to the Internet destination
				log(Level.FINEST,"Destination "+message.getToNode().getAddress()+" in ProxyTunnel not reachable, blocking  in tunnel " + getLocalAddress(), e);
				blockPeer(message);
				continue;
			} catch (Exception e) {
				log(Level.FINEST, e);
				closeTunnel();
				return;
			}
		}
	}

	protected synchronized void writePayloadToTunnel(ProxyPayload payload) {
		try {
			if(out == null){
				return;
			}
	                    
			//ProxyPayloadWrapper str = new ProxyPayloadWrapper(ProxyPayload.serializePayload(payload));
	                    
			// System.out.println("out: " + str);
			out.writeUnshared(payload);
			out.flush();
			out.reset();
		} catch (Exception e) {
			log(Level.FINEST, e);
			addPayloadToBacklog(payload);
			closeTunnel();
		}
	}

	private synchronized void handleBacklog() {
		if (backlog != null) {
			while (!backlog.isEmpty() && running == true) {
				writePayloadToTunnel(backlog.poll());
			}
			backlog = null;
		}
	}

	public synchronized void closeTunnel() {
		if(inRecovery){
			return;
		}
		inRecovery = true;
		try {
			if(socket != null && socket.getRemoteSocketAddress() != null)
				log(Level.INFO, "Closing Tunnel for " + socket.getRemoteSocketAddress().toString() + " to " + getLocalAddress());
			else
				log(Level.INFO, "Closing Tunnel for " + getLocalAddress());
			running = false;
			if(!mainThread.getName().contains("sleep"))
				mainThread.interrupt();
			if(in!=null && !socket.isClosed())
				in.close();
			if(out!=null && !socket.isClosed())
				out.close();
			if(socket!=null && !socket.isClosed())
				socket.close();
			
		} catch (Exception e) {
			log(Level.FINEST, e);
		}
		finally{
			in = null;
			out = null;
			socket = null;
		}
		startTimeout();
	}

	private void shutdown() {
			running = false;
			if(gatewayCommunication != null){
			gatewayCommunication.shutdown();
			gatewayCommunication = null;
			}
			if(socket != null){
				try {
					in.close();
					out.close();
					socket.close();
				} catch (IOException e) {
	//				e.printStackTrace();
				}
				finally{
					in = null;
					out = null;
					socket = null;
				}
			}
			parent.removeTunnel(this);
		}

	private void addPayloadToBacklog(ProxyPayload payload) {
		if (backlog == null) {
			backlog = new LinkedList<ProxyPayload>();
		}
		backlog.add(payload);
	}

	protected synchronized void setSocket(Socket newSocket, ObjectInputStream inputStream, ObjectOutputStream outputStream) {
		
		if (timeoutThread != null) {
			log(Level.FINEST, "Setting new socket, interrupting timeout thread!");
			timeoutThread.interrupt();
		}
		else{
			log(Level.SEVERE, "Setting new socket, timeout thread not found!!!");
		}
		inRecovery = false;
		running = true;
		socket = newSocket;
		in = inputStream;
		out = outputStream;
		createAndStartProxyTunnelThread();
		createAndStartKeepAliveThread();
	}

	private void startTimeout() {
		if(timeoutThread != null){
			return;
		}
		int id = random.nextInt();
		log(Level.INFO, "Timeout thread "+ id + " started for " + getLocalAddress());
		timeoutThread = new Thread(new TimeOutThread(id), "sleeperThread"+ id + " for:"
				+ gatewayCommunication.getLocalSensibleThingsNode().getAddress());
		timeoutThread.start();
	}

	protected Long getSessionID() {
		return sessionID;
	}

	public SensibleThingsNode getLocalAddress() {
		return gatewayCommunication.getLocalSensibleThingsNode();
	}

	public String getRemoteAddress() {
		if(socket != null)
			return socket.getRemoteSocketAddress().toString();
		else if(inRecovery){
			return "recovering";
		}
		else return null;
	}

	private void sendKeepAlive() {
		if(System.currentTimeMillis()-lastRecievedMessage > KEEPALIVE_TIME){
		ProxyPayload p = new ProxyPayload(getSessionID(), null, null, null);
		writePayloadToTunnel(p);
		}
	}
	
	public void blockPeer(Message message) {
		ProxyPayload p = new ProxyPayload(getSessionID(),null,message.getFromNode(), message.getToNode());
		writePayloadToTunnel(p);
		
	}

	public boolean isAlive() {
		sendKeepAlive();
		return System.currentTimeMillis() < lastRecievedMessage+ KEEPALIVE_TIME*2;
	}
	final class TimeOutThread implements Runnable {
		private int instance;
		public TimeOutThread(int id) {
			instance = id;
		}

		public void run() {
			try {
				Thread.sleep(RECONNECTION_TIMEOUT);
				log(Level.INFO, "Timeout thread"+ instance + " expired for " + getLocalAddress() + ", closing socket");
				if (socket == null) {
					parent.removeTunnel(ProxyTunnel.this);
					gatewayCommunication.shutdown();
					running = false;
					mainThread = null;
					timeoutThread = null;
					keepAliveThread = null;
					
					if(in!= null)
						in.close();
					if(out!=null)
						out.close();
					if(socket!=null)
						socket.close();
				}
			} catch (InterruptedException e) {
				// Do nothing, let thread die silently
				log(Level.FINER, "Timeout thread"+ instance + " for " + getLocalAddress() + " interrupted, tunnel resumed");
				timeoutThread = null;
				// e.printStackTrace();
			} catch (IOException e) {
				 log(Level.INFO, "IOError in timeout thread"+ instance + ", trace:");
				log(Level.FINEST, e);
			}

		}
	}
	
	private final class KeepAliveThread implements Runnable {
		
		
		public void run() {
			log(Level.FINER, "KeepaliveThread " + Thread.currentThread().getName() + " started.");
			while(running){
				if(System.currentTimeMillis() < lastRecievedMessage+KEEPALIVE_TIME){
					try {
						Thread.sleep(KEEPALIVE_TIME/2);
					} catch (InterruptedException e) {
						if(keepAliveThread != Thread.currentThread()){
							log(Level.FINER, "KeepaliveThread " + Thread.currentThread().getName() + " was replaced.");
							return;
						}
						//e.printStackTrace();
					}
					continue;
				}
				else if (System.currentTimeMillis() < lastRecievedMessage+KEEPALIVE_TIME*2){
					sendKeepAlive();
				}
				else{
					closeTunnel();
				}
			}
		}
	}
}