/*
 * Copyright 2013 The SensibleThings Consortium
 * This file is part of The SensibleThings Platform.
 *
 * The SensibleThings Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The SensibleThings Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The SensibleThings Platform.  If not, see <http://www.gnu.org/licenses/>.
 */

package se.sensiblethings.addinlayer.extensions.proxy.rudp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;

import se.sensiblethings.addinlayer.extensions.proxy.ProxyExtension;
import se.sensiblethings.addinlayer.extensions.proxy.ProxyTunnel;
import se.sensiblethings.disseminationlayer.communication.Communication;
import se.sensiblethings.disseminationlayer.communication.DestinationNotReachableException;
import se.sensiblethings.disseminationlayer.communication.Message;
import se.sensiblethings.disseminationlayer.communication.proxy.ProxyPayload;
import se.sensiblethings.disseminationlayer.communication.rudp.RUDPCommunication;
import se.sensiblethings.disseminationlayer.communication.rudp.socket.datagram.RUDPDatagram;


public class RudpProxyTunnel extends ProxyTunnel implements Runnable {

	
	public RudpProxyTunnel(ProxyExtension parent, long sessionID,
			Socket socket, ObjectInputStream in, ObjectOutputStream out) {
		super(parent, sessionID, socket, in, out);
	}

	@Override
	protected Communication createCommunicationLayer() {
		return new RudpGatewayCommunication();
	}

	private class RudpGatewayCommunication extends RUDPCommunication {
		public RudpGatewayCommunication() {
			super(0);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * se.mediasense.disseminationlayer.communication.rudp.RUDPCommunication
		 * #sendMessage(se.mediasense.disseminationlayer.communication.Message)
		 */
		@Override
		public void sendMessage(Message message) throws DestinationNotReachableException {
			try {
				super.sendMessage(message);
			}  catch (RuntimeException e) {
				 log(Level.INFO, "Irrecoverable runtime error in one proxyTunnel communication-layer, printing stacktrace and closing tunnel");
				log(Level.FINEST, e);
				closeTunnel();
			}
		}

		@Override
		protected void handleData(RUDPDatagram dgram) {

			Message message = messageSerializer.deserializeMessage(dgram.getData());



			// Instead of sending the message to the PostOffice we send it over
			// the tunnel
			byte[] data = messageSerializer.serializeMessage(message);
			if (data == null) {
				System.out.println("OUT:    serializeMessage returned null!!!");
			}
			ProxyPayload p = new ProxyPayload(getSessionID(), data, message.getFromNode(),
					message.getToNode());
			writePayloadToTunnel(p);
		}
	}

}
