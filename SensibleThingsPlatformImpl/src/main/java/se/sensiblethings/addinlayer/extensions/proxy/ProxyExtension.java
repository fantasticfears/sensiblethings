/*
 * Copyright 2013 The SensibleThings Consortium
 * This file is part of The SensibleThings Platform.
 *
 * The SensibleThings Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The SensibleThings Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The SensibleThings Platform.  If not, see <http://www.gnu.org/licenses/>.
 */

package se.sensiblethings.addinlayer.extensions.proxy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import se.sensiblethings.addinlayer.extensions.Extension;
import se.sensiblethings.disseminationlayer.communication.proxy.ProxyPayload;
import se.sensiblethings.interfacelayer.SensibleThingsPlatform;

public abstract class ProxyExtension implements Extension, Runnable {

	final class TimeOutConnectionOnExceptionHandler implements
			UncaughtExceptionHandler {

		public void uncaughtException(Thread thread, Throwable exception) {
			if (thread == ProxyExtension.this.thread) {
				if (runProxy) {
					log(Level.WARNING,
							"ProxyTunnel caught an unhandled exception while running, stopping tunnel");
					stopAddIn();
					startAddIn();
				}
			}
		}
	}

	TimeOutConnectionOnExceptionHandler timeOutConnectionOnExceptionHandler = new TimeOutConnectionOnExceptionHandler();
	protected int proxyPort = 14523;
	protected boolean runProxy = true;
	private Map<Long, ProxyTunnel> proxyTunnels = new ConcurrentHashMap<Long, ProxyTunnel>();
	private ServerSocket serverSocket = null;
	private Random ra = new Random();
	protected Thread thread;
	private ExecutorService executorPool;
	private static Logger logger  = Logger.getLogger(ProxyExtension.class.getName());;
	public static void SetFileLogger() {
		ProxyTunnel.SetFileLogger();
		// Initial Setup
		// Filelogger, for debugging purposes
		//logger  = Logger.getLogger(ProxyExtension.class.getName());
		// Disables Console Output for Errors
		logger.setUseParentHandlers(false);
		FileHandler fh;
		try {
			fh = new FileHandler("./log/proxyExtension%g.log", 1024 * 1024 * 3,
					5);
			fh.setLevel(Level.ALL);
			logger.addHandler(fh);
			logger.setLevel(Level.ALL);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
			logger.log(Level.INFO, "Logging Started.");
		} catch (IOException ex) {
			log(Level.SEVERE, null, ex);
		} catch (SecurityException ex) {
			log(Level.SEVERE, null, ex);
		}
	}

	private static void log(Level logtype, String logtext, Exception logex) {
		logger.log(logtype, logtext,
				logex);
	}

	protected static void log(Level logtype, String logtext) {
		logger.log(logtype, logtext);
	}

	private static void log(Level logtype, Exception e) {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		e.printStackTrace(new PrintStream(output, true));
		log(logtype, output.toString());
	}

	public void loadAddIn(SensibleThingsPlatform platform) {
		runProxy = false;
		// Nothing to Load
	}

	public void startAddIn() {
		if (runProxy == false) {
			runProxy = true;
			executorPool = Executors.newCachedThreadPool();
			thread = new Thread(this);
			thread.setName("ProxyExtension-Main Thread");
			thread.start();
		}
	}

	public synchronized void stopAddIn() {
		if (runProxy == false) {
			return;
		}
		runProxy = false;
		executorPool.shutdown();
		// Stop the Thread
		try {
			thread.interrupt();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Stop the Server Socket
		try {
			serverSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Stop all Proxy Tunnels
		try {
			for (ProxyTunnel pt : proxyTunnels.values()) {
				pt.closeTunnel();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		executorPool.shutdownNow();
	}

	public void unloadAddIn() {
		proxyTunnels.clear();
	}

	public void run() {
		try {
			serverSocket = createSocket();

			while (runProxy) {
				Socket socket = serverSocket.accept();
				handleClient(socket);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected abstract ServerSocket createSocket() throws IOException;

	/**
	 * For error handling of sockets
	 * 
	 * @param client
	 */
	private void handleClient(final Socket client) {

		executorPool.execute(new Runnable() {
			public void run() {
				try {
					handleClientSocket(client);

				} catch (ClassNotFoundException e) {
					log(Level.WARNING,
							"Client connected from:"
									+ client.toString()
									+ " is using an incompatible version of proxy connection");
					try {
						client.close();
					} catch (IOException e1) {

						log(Level.WARNING,
								"Could not close socket " + client.toString()
										+ " properly");
						e1.printStackTrace();
					}
					return;
					// e.printStackTrace();
				} catch (IOException e) {
					log(Level.WARNING,
							"Unknown IO exception when establishing proxy tunnel, closing, printing stacktrace:");
					log(Level.WARNING, e);
					try {
						client.close();
					} catch (IOException e1) {
						System.out.println("Could not close socket properly");
						e1.printStackTrace();
					}
				}
			}
		});
	}

	/**
	 * Handles incomming socket
	 * 
	 * @param client
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void handleClientSocket(Socket client)
			throws IOException, ClassNotFoundException {
		log(Level.INFO, "Proxy Client connected from:"
				+ client.getRemoteSocketAddress().toString());

		// Read initial payload
		// Init reader/writer
		ObjectInputStream in;
		ObjectOutputStream out;
		ProxyPayload p;
		try {
			//Utan Zip
			OutputStream os = client.getOutputStream();
			out = new ObjectOutputStream(os);
			InputStream is = client.getInputStream();
			in = new ObjectInputStream(is);
			
			
			//Testar med GZIP
			//Verkar inte funka
			/*
			OutputStream os = client.getOutputStream();
			GZIPOutputStream gos = new GZIPOutputStream(os);			
			out = new ObjectOutputStream(gos);			
			InputStream is = client.getInputStream();
			GZIPInputStream gis = new GZIPInputStream(is);    
			in = new ObjectInputStream(gis);
			*/

			p = getPayload(in);
		} catch (NullPointerException e) {
			log(Level.WARNING, "odd connection request");
			log(Level.WARNING, e);
			client.close();
			return;
		}

		// Check if this is a reestablishment of a previous tunnel
		ProxyTunnel tunnel;
//		synchronized(proxyTunnels){
		log(Level.FINEST, "Client contained sessionkey " + p.getSessionID());
		if (proxyTunnels.containsKey(p.getSessionID())) {
			log(Level.FINEST, "Matching tunnel exists " + p.getSessionID());
			tunnel = proxyTunnels.get(p.getSessionID());
			// In case reconnecting node have the wrong local ip something has
			// gone wrong, and it should not be allowed to reconnect
			if (p.getTo() != null && p.getTo().getAddress().contentEquals(tunnel.getLocalAddress().getAddress())) {
				tunnel.setSocket(client, in, out);
				log(Level.INFO, "Proxy Client " + tunnel.getLocalAddress()
						+ " from " + tunnel.getRemoteAddress() + "  Resumed!");
				return;
			}
			log(Level.INFO,
					"Proxy Client sessionID is already in use!\nGenerating new ID");
		}
		if(p.getSessionID() == 0){
			log(Level.FINEST, "Starting new tunnel");
		}else{
			log(Level.FINEST, "Matching tunnel did not exist " + p.getSessionID() + " creating new tunnel");
		}
		// Create a new tunnel endpoint on this end
		tunnel = createTunnel(client, in, out);

		proxyTunnels.put(tunnel.getSessionID(), tunnel);
		
		log(Level.INFO, "Proxy Client " + tunnel.getLocalAddress() + " from "
				+ tunnel.getRemoteAddress() + "  Started!");
//		}
		}

	protected abstract ProxyTunnel createTunnel(Socket client,
			ObjectInputStream in, ObjectOutputStream out);

	/**
	 * Ineffective id-selection method for large populations of tunnels
	 * 
	 * @return a new unused id
	 */
	protected long getUnusedSessionID() {
		long id;
		do {
			id = ra.nextLong();
		} while (proxyTunnels.containsKey(ra) || id == 0);
		return id;
	}

	private ProxyPayload getPayload(ObjectInputStream in) throws IOException,
			ClassNotFoundException {
		// Read first payload
		ProxyPayload payload = (ProxyPayload) in.readUnshared();

		// System.out.println("inc: " + str);

		// In case we have a mismatch in \n being \r\n or just \n but
		// interpreted as \n\n we can get an empty string
		if (payload == null) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
			} // TODO optimize delay?
			payload = (ProxyPayload) in.readUnshared();
		}

		return payload;
	}

	public void removeTunnel(ProxyTunnel proxyTunnel) {
		// ProxyTunnel tunnel = proxyTunnels.remove(proxyTunnel.getSessionID());
//		synchronized(proxyTunnels){
			proxyTunnels.remove(proxyTunnel.getSessionID());
//		}
		}

	public String printTunnels() {
		String s = "";
		Set<Entry<Long, ProxyTunnel>> set;
//		synchronized(proxyTunnels){
		set = proxyTunnels.entrySet();
//		}
		for (Iterator<Entry<Long, ProxyTunnel>> iterator = set.iterator(); iterator
				.hasNext();) {
			Entry<Long, ProxyTunnel> entry = (Entry<Long, ProxyTunnel>) iterator
					.next();
			s = s + "TunnelID: " + entry.getKey() + " is connected as "
					+ entry.getValue().getLocalAddress() + " from "
					+ entry.getValue().getRemoteAddress() + "\n";
		}
		return s;
	}

	public void cleanUp() {
		//System.out.println("ProxyExtension is running cleanup");
		Set<Entry<Long, ProxyTunnel>> set;
//		synchronized(proxyTunnels){
		set = proxyTunnels.entrySet();
//		}
		for (Iterator<Entry<Long, ProxyTunnel>> iterator = set.iterator(); iterator
				.hasNext();) {
			Entry<Long, ProxyTunnel> entry = (Entry<Long, ProxyTunnel>) iterator
					.next();
			ProxyTunnel tunnel = entry.getValue();
			if (!tunnel.isAlive()) {
			}
		}
	}
}