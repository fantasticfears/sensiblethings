/*
 * Copyright 2013 The SensibleThings Consortium
 * This file is part of The SensibleThings Platform.
 *
 * The SensibleThings Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The SensibleThings Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The SensibleThings Platform.  If not, see <http://www.gnu.org/licenses/>.
 */

package se.sensiblethings.addinlayer.extensions.proxy.rudp;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import se.sensiblethings.addinlayer.extensions.proxy.ProxyExtension;
import se.sensiblethings.addinlayer.extensions.proxy.ProxyTunnel;

public class RudpProxyExtension extends ProxyExtension {

	private int proxyPort = 14525;


	@Override
	protected ProxyTunnel createTunnel(Socket client, ObjectInputStream in, ObjectOutputStream out) {
		RudpProxyTunnel tunnel;
		tunnel = new RudpProxyTunnel(this, getUnusedSessionID(),client, in, out);
		return tunnel;
	}

	@Override
	protected ServerSocket createSocket() throws IOException {

		ServerSocket serverSocket = new ServerSocket(proxyPort);
	    return serverSocket;
	}

}
