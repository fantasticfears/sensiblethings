/*
 * Copyright 2013 The SensibleThings Consortium
 * This file is part of The SensibleThings Platform.
 *
 * The SensibleThings Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The SensibleThings Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The SensibleThings Platform.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.sensiblethings.addinlayer.extensions.proxy.ssl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;


import se.sensiblethings.addinlayer.extensions.proxy.ProxyExtension;
import se.sensiblethings.addinlayer.extensions.proxy.ProxyTunnel;

public class SslProxyExtension extends ProxyExtension {

    private int proxyPort = 14524;
    SSLServerSocketFactory ssocketFactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
    final String[] enabledCipherSuites = {"SSL_DH_anon_WITH_RC4_128_MD5"};

    protected ProxyTunnel createTunnel(Socket client, ObjectInputStream in, ObjectOutputStream out) {
        ProxyTunnel tunnel = null;
        tunnel = new SslProxyTunnel(this, getUnusedSessionID(), client, in, out);
        return tunnel;
    }

    @Override
    protected ServerSocket createSocket() throws IOException {
        SSLServerSocket serverSocket = (SSLServerSocket) ssocketFactory.createServerSocket(proxyPort);

        serverSocket.setEnabledCipherSuites(enabledCipherSuites);
        return serverSocket;
    }
}
