/*
 * Copyright 2013 The SensibleThings Consortium
 * This file is part of The SensibleThings Platform.
 *
 * The SensibleThings Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The SensibleThings Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The SensibleThings Platform.  If not, see <http://www.gnu.org/licenses/>.
 */

package se.sensiblethings.addinlayer.extensions.cache;

import java.util.HashMap;




import se.sensiblethings.addinlayer.extensions.Extension;
import se.sensiblethings.disseminationlayer.disseminationcore.DisseminationCore;
import se.sensiblethings.disseminationlayer.disseminationcore.GetResponseListener;
import se.sensiblethings.interfacelayer.SensibleThingsNode;
import se.sensiblethings.interfacelayer.SensibleThingsPlatform;


public class CacheExtension implements Extension, GetResponseListener{

	private SensibleThingsPlatform platform = null;
	private DisseminationCore core = null;
	
	private HashMap<String, Long> timeCache = new HashMap<String, Long>();
	private HashMap<String, String> valueCache = new HashMap<String, String>(); 
	
	private GetResponseListener savedGetResponseListener = null;
	
	private boolean runAddIn = true;
	
	public void loadAddIn(SensibleThingsPlatform platform) {
		this.platform = platform;	
		this.core = platform.getDisseminationCore();
	}

	public void startAddIn() {
		runAddIn = true;
		Thread t = new HijackThread();
		t.start();		
	}

	public void stopAddIn() {
		runAddIn = false;		
	}

	public void unloadAddIn() {
		timeCache.clear();
		valueCache.clear();		
	}

	/**
	 * This performs a get, but checks the cache beforehand.
	 * If the value exists in the cache and was retrieved within the freshness threshold, the cached value is returned instead 
	 * @param uci the UCI to get
	 * @param node the SensibleThingNode to get it from
	 * @param threshold the freshness threshold in milliseconds
	 */
	public void get(String uci, SensibleThingsNode node, int threshold) {
						
		//Get values from cache and sanity check
		Long cachedTime = timeCache.get(uci);
		String cachedValue = valueCache.get(uci);		
		if(cachedTime == null || cachedValue == null){
			platform.get(uci, node);
			return;
		}

		//If it is old
		if(System.currentTimeMillis() - cachedTime > threshold){
			//Make a new call
			platform.get(uci, node);
		} else {
			//Otherwise, return the cached value!
			platform.getDisseminationCore().callGetResponseListener(uci, cachedValue.getBytes(), node);
		}				
	}
	

	public void getResponse(String uci, String value, SensibleThingsNode node) {
		//Cache the values!
		valueCache.put(uci, value);
		timeCache.put(uci, System.currentTimeMillis());
		
		//Make a normal call
		if(savedGetResponseListener != null){
			savedGetResponseListener.getResponse(uci, value, node);
		}				
	}
	
	//This class will hijack the getResponses and cache them!
	private class HijackThread extends Thread{
		
		@Override
		public void run() {
			while(runAddIn){
				try {
					
					Thread.sleep(500);
				
					if(core.getGetResponseListener() != null && core.getGetResponseListener() != CacheExtension.this){						
						savedGetResponseListener = core.getGetResponseListener();						
					}
					core.setGetResponseListener(CacheExtension.this);//Take over the listener!

				} catch (Exception e) {
					//e.printStackTrace();					
				}
			}
		}				
	}
}
