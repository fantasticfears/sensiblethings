/*
 * Copyright 2013 The SensibleThings Consortium
 * This file is part of The SensibleThings Platform.
 *
 * The SensibleThings Platform is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The SensibleThings Platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The SensibleThings Platform.  If not, see <http://www.gnu.org/licenses/>.
 */

package se.sensiblethings.addinlayer.extensions.authentication;

import java.util.HashMap;

import se.sensiblethings.addinlayer.extensions.Extension;
import se.sensiblethings.disseminationlayer.communication.Communication;
import se.sensiblethings.disseminationlayer.communication.Message;
import se.sensiblethings.disseminationlayer.disseminationcore.DisseminationCore;
import se.sensiblethings.disseminationlayer.disseminationcore.MessageListener;
import se.sensiblethings.interfacelayer.SensibleThingsNode;
import se.sensiblethings.interfacelayer.SensibleThingsPlatform;

public class AuthenticationExtension implements Extension, MessageListener {

	SensibleThingsPlatform platform = null;
	DisseminationCore core = null;
	Communication communication = null;
	
	AuthenticationListener listener = null;
		
	HashMap<String, String> secrets = new HashMap<String, String>();
	
	public AuthenticationExtension(AuthenticationListener listener) {
		this.listener = listener;
	}
	
	public void loadAddIn(SensibleThingsPlatform platform) {
		this.platform = platform;
		this.core = platform.getDisseminationCore();
		this.communication = core.getCommunication();
		
		//Register our own message types in the post office
		communication.registerMessageListener(AuthenticatedGetMessage.class.getName(), this);
		communication.registerMessageListener(AuthenticatedSetMessage.class.getName(), this);	
				
	}

	public void startAddIn() {		
	}

	public void stopAddIn() {
	}

	public void unloadAddIn() {
		
	}
	

	//Auth a secret
	public void autenticate(String uci, String secret){
		secrets.put(uci, secret);		
	}


	public void get(String uci, SensibleThingsNode node, String secret) {
		try {		
			AuthenticatedGetMessage authGetMessage = new AuthenticatedGetMessage(uci, secret, node, communication.getLocalSensibleThingsNode());		
			communication.sendMessage(authGetMessage);
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	public void set(String uci, String value, SensibleThingsNode node, String secret) {
		try {		
			AuthenticatedSetMessage authSetMessage = new AuthenticatedSetMessage(uci, value, secret, node, communication.getLocalSensibleThingsNode());		
			communication.sendMessage(authSetMessage);
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
		
	public void handleMessage(Message message) {
				
        if(message instanceof AuthenticatedGetMessage) {
        	AuthenticatedGetMessage authGetMessage = (AuthenticatedGetMessage) message;
        	SensibleThingsNode source = authGetMessage.getFromNode();
        	String uci = authGetMessage.uci;
        	String secret = authGetMessage.secret;
        	
        	//Check if secret is ok        	
        	String storedSecret = secrets.get(uci);        	
        	if(storedSecret != null){
            	if(storedSecret.equalsIgnoreCase(secret)){
                	if(listener != null){
                		listener.authenticatedGetEvent(source, uci);
                	}        		
            	}  else {        		
            		core.callGetEventListener(source, uci);        		        		
            	}       		
        	} else {        		
        		core.callGetEventListener(source, uci);        		        		
        	}
        	
        	
        } else if(message instanceof AuthenticatedSetMessage) {
        	AuthenticatedSetMessage authSetMessage = (AuthenticatedSetMessage) message;
        	SensibleThingsNode source = authSetMessage.getFromNode();
        	String uci = authSetMessage.uci;
        	String value = authSetMessage.value;
        	String secret = authSetMessage.secret;
        	        	
        	//Check if secret is ok
        	if(secrets.get(uci).equalsIgnoreCase(secret)){
            	if(listener != null){
            		listener.authenticatedSetEvent(source, uci, value);
            	}        		
        	} else {        		
        		core.callSetEventListener(source, uci, value.getBytes());        		        		
        	}
        	
        }
		
	}
	
}
